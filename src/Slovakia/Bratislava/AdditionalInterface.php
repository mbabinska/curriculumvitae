<?php

namespace Slovakia\Bratislava;

/**
 * Interface AdditionalInterface
 */
interface AdditionalInterface
{
    /**
     * @return array
     */
    public function getAdditionalInfo();

    /**
     * Add an Info item to the list of Additional Info
     *
     * @param string $type
     * @param array $info
     *
     * @return $this
     */
    public function addAdditionalInfo($type, $info);
}