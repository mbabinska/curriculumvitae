<?php
namespace Slovakia\Bratislava;

/**
 * Interface EducationInterface
 */
interface EducationInterface
{
    /**
     * @return array
     */
    public function getEducations();

    /**
     * Add an Education item to the list of Education history
     *
     * @param string $type
     * @param array  $education
     *
     * @return array
     */
    public function addEducation($type, array $education);
}