<?php
namespace Slovakia\Bratislava;

/**
 * Interface SkillsInterface
 */
interface SkillsInterface
{
    /**
     * @return array
     */
    public function getSkills();

    /**
     * Add a Skill item to the list of Skills
     *
     * @param  string $type
     * @param array   $skills
     *
     * @return $this
     */
    public function addSkillSet($type, array $skills);
}