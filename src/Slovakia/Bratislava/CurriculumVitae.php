<?php
namespace Slovakia\Bratislava;

/**
 * Class CurriculumVitae
 */
class CurriculumVitae implements EducationInterface, WorkExperienceInterface, SkillsInterface, InterestsInterface, AdditionalInterface
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var array
     */
    private $educationHistory = [];

    /**
     * @var array
     */
    private $workExperience = [];

    /**
     * @var array
     */
    private $skills = [];

    /**
     * @var array
     */
    private $interests = [];

    /**
     * @var array
     */
    private $additionalInfo = [];

    /**
     * CurriculumVitae constructor.
     *
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $phone
     */
    public function __construct($firstName, $lastName, $email, $phone)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phone = $phone;
    }

    /**
     * @return array
     */
    public function getEducations()
    {
        return $this->educationHistory;
    }

    /**
     * Add an Education item to the list of Education history
     *
     * @param string  $type
     * @param array   $education
     *
     * @return array
     */
    public function addEducation($type, array $education)
    {
        $this->educationHistory[$type] = $education;

        return $this->educationHistory;
    }

    /**
     * @return array
     */
    public function getWorkExperience()
    {
        return $this->workExperience;
    }

    /**
     * Add an Experience item to the list of Experience history
     *
     * @param string $type
     * @param array  $experience
     *
     * @return $this
     */
    public function addWorkExperience($type, array $experience)
    {
        $this->workExperience[$type][] = $experience;

        return $this;
    }

    /**
     * @return array
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Add a Skill item to the list of Skills
     *
     * @param string $type
     * @param array  $skills
     *
     * @return $this
     *
     */
    public function addSkillSet($type, array $skills)
    {
        $this->skills[$type] = $skills;

        return $this;
    }

    /**
     * @return array
     */
    public function getInterests()
    {
        return $this->interests;
    }

    /**
     * Add an Interest item to the list of Interests
     *
     * @param string $interest
     *
     * @return $this
     */
    public function addInterest($interest)
    {
        $this->interests[] = $interest;

        return $this;
    }

    /**
     * @return array
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }

    /**
     * Add an Info item to the list of Additional Info
     *
     * @param string $type
     * @param array  $info
     *
     * @return $this
     */
    public function addAdditionalInfo($type, $info)
    {
        $this->additionalInfo[$type][] = $info;

        return $this;
    }

    /**
     * Print Curriculum Vitae
     *
     * @param CurriculumVitae $cv
     */
    public function printCurriculumVitae(CurriculumVitae $cv)
    {
        echo "<pre>";
        var_dump($cv);
    }
}